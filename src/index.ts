import { SortingsRunner } from './algorithms_and_structures/sortings/Runner';
import { HashTableRunner } from './algorithms_and_structures/hash_tables/Runner';
import { TreesRunner } from './algorithms_and_structures/trees/Runner';

new SortingsRunner().run(); // sortings
new HashTableRunner().run(); // hashTable
new TreesRunner().run(); // trees
