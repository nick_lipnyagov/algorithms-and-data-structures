import { Node } from '../node';

export class BinarySearchTree {
    root: Node | undefined;

    constructor(arr: number[]) {
        this.init(arr);
    }

    create(node: number) {
        if (!this.root) {
            this.root = new Node(node);
        } else {
            let current: Node = this.root;

            while (true) {
                if (node < current.value) {
                    if (current.left) {
                        current = current.left;
                    } else {
                        current.left = new Node(node);
                        break;
                    }
                } else if (node > current.value) {
                    if (current.right) {
                        current = current.right;
                    } else {
                        current.right = new Node(node);
                        break;
                    }
                } else {
                    break;
                }
            }
        }
    }

    init(arr: number[]) {
        for (const item of arr) {
            this.create(item);
        }
    }

    search(value: number) {
        return this.contains(this.root!, value);
    }

    contains(current: Node, value: number): boolean {
        if (current == null) {
            return false;
        }
        if (value === current.value) {
            return true;
        }
        return value < current.value
          ? this.contains(current!.left!, value)
          : this.contains(current!.right!, value);
    }
}
