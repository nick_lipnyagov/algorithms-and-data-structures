import { IRunnable } from '../../interfaces/IRunnable';

import { BinarySearchTree } from './binary_tree/binaryTree';
import { AvlTree } from './avl/avlTree';

export class TreesRunner implements IRunnable {
    run(): void {
        const binaryTree = new BinarySearchTree([1,5,2,10,7]);
        binaryTree.search(5);
        console.log('Binary tree search executed');

        const avlTree = new AvlTree(); // avl tree implementation
        avlTree.addElement(1);
        avlTree.addElement(5);
        avlTree.addElement(2);
        avlTree.addElement(10);
        avlTree.addElement(7);
        avlTree.contains(5);
        console.log('Avl tree search executed');
    }
}