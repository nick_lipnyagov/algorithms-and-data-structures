export class Node {
    public value: number;
    public left?: Node;
    public right?: Node;
    public level?: number;
    public height?: number;

    constructor(value: number) {
        this.value = value;
    }

    public get leftHeight(): number {
        if (this.left === null) {
            return -1;
        }
        return this.left?.height || 0;
    }

    public get rightHeight(): number {
        if (this.right === null) {
            return -1;
        }
        return this.right?.height || 0;
    }

    public rotateRight(): Node {
        //     b                           a
        //    / \                         / \
        //   a   e -> b.rotateRight() -> c   b
        //  / \                             / \
        // c   d                           d   e
        const other = this.left;
        this.left = other?.right;
        other!.right = this;

        this.height = Math.max(this.leftHeight, this.rightHeight) + 1;
        other!.height = Math.max(other!.leftHeight, this.height) + 1;

        return other!;
    }

    public rotateLeft(): Node {
        //   a                              b
        //  / \                            / \
        // c   b   -> a.rotateLeft() ->   a   e
        //    / \                        / \
        //   d   e                      c   d
        const other = this.right;
        this.right = other!.left;
        other!.left = this;

        this.height = Math.max(this.leftHeight, this.rightHeight) + 1;
        other!.height = Math.max(other!.rightHeight, this.height) + 1;

        return other!;
    }
}