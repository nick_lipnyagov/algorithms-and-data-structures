import { Node } from '../node';
import { BalanceState } from './balanceState';

export class AvlTree {
    private root: Node | null = null;
    private size: number = 0;

    public getSize(): number {
        return this.size;
    }

    private compare(a: number, b: number): number {
        if (a > b) {
            return 1;
        } else if (a < b) {
            return -1;
        }
        return 0;
    }

    public addElement(value: number): void {
        this.root = this.addNode(value, this.root);
        this.size++;
    }

    private addNode(value: number, root: Node | null): Node {
        if (root === null) {
            return new Node(value);
        }

        if (this.compare(value, root.value) < 0) {
            root.left = root.left? this.addNode(value, root.left!): new Node(value);
        } else if (this.compare(value, root.value) > 0) {
            root.right = root.right ? this.addNode(value, root.right!): new Node(value);
        } else {
            this.size--;
            return root;
        }

        root.height = Math.max(root.leftHeight, root.rightHeight) + 1;
        const balanceState = this.getBalanceState(root);

        // tree rebalancing
        if (balanceState === BalanceState.UNBALANCED_LEFT) {
            if (this.compare(value, root.left!.value) < 0) {
                root = root.rotateRight();
            } else {
                root.left = root.left?.rotateLeft();
                return root.rotateRight();
            }
        }

        if (balanceState === BalanceState.UNBALANCED_RIGHT) {
            if (this.compare(value, root.right!.value) > 0) {
                root = root.rotateLeft();
            } else {
                root.right = root.right!.rotateRight();
                return root.rotateLeft();
            }
        }

        return root;
    }

    private getBalanceState(node: Node): BalanceState {
        const heightDiff = node.leftHeight - node.rightHeight;
        switch (heightDiff) {
            case -2:
                return BalanceState.UNBALANCED_RIGHT;
            case -1:
                return BalanceState.SLIGHTLY_UNBALANCED_RIGHT;
            case 1:
                return BalanceState.SLIGHTLY_UNBALANCED_LEFT;
            case 2:
                return BalanceState.UNBALANCED_LEFT;
            default:
                return BalanceState.BALANCED;
        }
    }

    public contains(value: number): boolean {
        if (this.root === null) {
            return false;
        }

        return !!this.getNode(value, this.root);
    }

    private getNode(value: number, root: Node): Node | null {
        const result = this.compare(value, root.value);

        if (result === 0) {
            return root;
        } else if (result < 0) {
            if (!root.left) {
                return null;
            }
            return this.getNode(value, root.left);
        } else if (result > 0) {
            if (!root.right) {
                return null;
            }
            return this.getNode(value, root.right);
        }

        return null;
    }
}
