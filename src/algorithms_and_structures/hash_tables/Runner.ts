import { IRunnable } from '../../interfaces/IRunnable';

import { HashTable } from './hashTable';

export class HashTableRunner implements IRunnable {
    run(): void {
        const hashTable = new HashTable(10);
        hashTable.insert('element1', 'value1');
        hashTable.insert('element2', 'value2');
        hashTable.insert('element3', 'value3');
        hashTable.get('element1');

        console.log(`Hash table get() executed`);
    }
}