import { LinkedList } from '../linked_list/linkedList';

export class HashTable {
    private size?: number;
    private table: LinkedList[] = [];

    constructor(size: number) {
        this.size = size;
        for (let i = 0; i < size; i++) {
            this.table[i] = new LinkedList();
        }
    }

    private hash(key: string): number {
        let id: number = 0;
        for (let i = 0; i < key.length; i++) {
            id += key.charCodeAt(i) * 100;
        }
        return (id % this.size!);
    }

    public insert(key: string, value: string) {
        const id: number = this.hash(key);
        const bucket: LinkedList = this.table[id];
        bucket.insert(value, key);
    }

    public get(key: string) {
        const id: number = this.hash(key);
        const bucket: LinkedList = this.table[id];
        if (!bucket.empty()) {
            const value: any = bucket.find(key);
            if (value) return value;
        }
        return 'not found';
    }
}
