export class QuickSort {

  private arr?: number[];

  public sort(arrayToSort: number[]): void {
    if (arrayToSort) {
      this.arr = arrayToSort;
      this.quicksort(0, this.arr.length - 1);
    }
  }

  private swap(i: number, j: number): void {
    const temp: number = this.arr![i];
    this.arr![i] = this.arr![j];
    this.arr![j] = temp;
  }


  private quicksort(low: number, high: number): void {
    let i: number = low;
    let j: number = high;
    const pivot: number = this.arr![Math.floor((low + high) / 2)];

    while (i <= j) {
      while (this.arr![i] < pivot) {
        i++;
      }

      while (this.arr![j] > pivot) {
        j--;
      }

      if (i <= j) {
        this.swap(i, j);
        i++;
        j--;
      }
    }

    if (low < j) {
      this.quicksort(low, j);
    }
    if (i < high) {
      this.quicksort(i, high);
    }
  }
}
