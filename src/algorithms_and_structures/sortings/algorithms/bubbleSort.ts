export class BubbleSort {

    private arr?: number[];

    public sort(arrayToSort: number[]): void {
        if (arrayToSort) {
            this.arr = arrayToSort;
            this.bubbleSort(arrayToSort);
        }
    }

    private bubbleSort(arr: number[]): void {
        for (let i: number = 0; i <= arr.length - 1; i++) {
            for (let j: number = i; j <= arr.length - 1; j++) {
                if (arr[i] > arr[j]) {
                    this.swap(i, j);
                }
            }
        }
    }

    private swap(i: number, j: number): void {
        const temp: number = this.arr![i];
        this.arr![i] = this.arr![j];
        this.arr![j] = temp;
    }
}
