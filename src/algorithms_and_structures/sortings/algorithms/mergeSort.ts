export class MergeSort {

  private temp: number[] = [];

  public sort(arrayToSort: number[]): void {
    if (arrayToSort) {
      this.mergeSort(arrayToSort, this.temp, 0, arrayToSort.length - 1);
    }
  }

  private mergeSort(arr: number[], temp: number[], left: number, right: number): void {
    if (left < right) {
      const center: number = Math.floor((left + right) / 2);

      this.mergeSort(arr, temp, left, center);
      this.mergeSort(arr, temp, center + 1, right);
      this.merge(arr, temp, left, center + 1, right);
    }
  }

  private merge(arr: number[], temp: number[], left: number, right: number, rightEnd: number): void {

    const leftEnd: number = right - 1;
    let k: number = left;

    while (left <= leftEnd && right <= rightEnd) {
      if (arr[left] <= arr[right]) {
        temp[k++] = arr[left++];
      } else {
        temp[k++] = arr[right++]
      }
    }

    while (left <= leftEnd) {
      temp[k++] = arr[left++];
    }

    while (right <= rightEnd) {
      temp[k++] = arr[right++];
    }

    for (let i: number = 0; i < temp.length; i++, rightEnd--) {
      arr[rightEnd] = temp[rightEnd];
    }

  }
}