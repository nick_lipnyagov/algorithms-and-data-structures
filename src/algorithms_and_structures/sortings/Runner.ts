import { IRunnable } from '../../interfaces/IRunnable';

import { QuickSort } from './algorithms/quickSort'
import { MergeSort } from './algorithms/mergeSort'
import { BubbleSort } from './algorithms/bubbleSort'

export class SortingsRunner implements IRunnable {
    run(): void {
        const bubbleSortInputArr: number[] = [];

        let quickSortInputArr: number[] = [];
        let mergeSortInputArr: number[] = [];

        for (let i = 0; i < 100; i++) {
            bubbleSortInputArr[i] = Math.floor(Math.random() * 100) + 1;
        }
        quickSortInputArr = [...bubbleSortInputArr];
        mergeSortInputArr = [...bubbleSortInputArr];

        console.time('bubbleSort');
        new BubbleSort().sort(bubbleSortInputArr);
        console.timeEnd('bubbleSort');

        console.time('quickSort');
        new QuickSort().sort(quickSortInputArr);
        console.timeEnd('quickSort');

        console.time('mergeSort');
        new MergeSort().sort(mergeSortInputArr);
        console.timeEnd('mergeSort');
    }
}