export class ListNode {
    private value: any;
    private key: string;
    private nextNode?: any;

    constructor(val: any, key: string) {
        this.key = key;
        this.value = val;
    }

    public getNext(): ListNode {
        return this.nextNode;
    }

    public setNext(node: ListNode) {
        this.nextNode = node;
    }

    public getKey(): string {
        return this.key;
    }

    public getValue(): any {
        return this.value;
    }
}
