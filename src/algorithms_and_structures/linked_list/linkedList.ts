import { ListNode } from './listNode';

export class LinkedList {
  private head: any = null;

  public empty(): boolean {
    return this.head ? false : true;
  }

  public getLastNode(): ListNode {
    let node: ListNode = this.head;
    while (node.getNext()) {
      node = node.getNext();
    }
    return node;
  }

  public find(key: string): any {
    let node: ListNode = this.head;
    while (node) {
      if (node.getKey() === key) {
        return node.getValue();
      }
      node = node.getNext();
    }
    return false;
  }

  public insert(value: any, key: string): void {
    const node: ListNode = new ListNode(value, key);
    if (!this.head) {
      this.head = node;
    } else {
      this.getLastNode().setNext(node);
    }
  }
}
