# Working with repository:

1. Install packages with command `npm i` 
2. Run the project: `npm run start` (in debug mode use following `npm run debug`)

If you want to run linter - use `npm run lint` command

# Implemented algorithms:

## Sortings:

1. Quick sort - recursive "devide and conqueror" by pivot element
2. Merge sort - recursive "devide and conqueror", then merge sub-arrays
3. Bubble sort - "bubble" analogy, the biggest (smallest) element comes up in the end

| Name                  | Best            | Average             | Worst               | Memory    | Stable    |
| --------------------- | :-------------: | :-----------------: | :-----------------: | :-------: | :-------: |
| **Quick sort**        | n&nbsp;log(n)   | n&nbsp;log(n)       | n<sup>2</sup>       | log(n)    | No        |
| **Merge sort**        | n&nbsp;log(n)   | n&nbsp;log(n)       | n&nbsp;log(n)       | n         | Yes       |
| **Bubble sort**       | n               | n<sup>2</sup>       | n<sup>2</sup>       | 1         | Yes       |

## Hash table

## Linked list

## Trees:

1. Binary tree
2. Avl tree
